package ru.breakingcode.repository.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import ru.breakingcode.service.model.BreakingcodeAuthority;

import javax.persistence.*;
import java.util.UUID;

@Data
@Entity
@Table(schema = "system", name = "users")
@EqualsAndHashCode(of = "uuid")
@AllArgsConstructor
@NoArgsConstructor
public class UserEntity {
    @Id
    private UUID uuid;
    private String login;
    private String name;
    private String surname;
    private String password;
    @Column(name = "role")
    private String role;
}
