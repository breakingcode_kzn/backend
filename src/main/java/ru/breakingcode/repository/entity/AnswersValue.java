package ru.breakingcode.repository.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@Entity
@Table(name = "answers_values")
@EqualsAndHashCode(of = "id")
public class AnswersValue {
    @Column(name = "answer_id")
    @Id
    private UUID answerId;
    private String value;

    public AnswersValue() {
    }

    public AnswersValue(UUID answerId, String value) {
        this.answerId = answerId;
        this.value = value;
    }

    public UUID getAnswerId() {
        return answerId;
    }

    public void setAnswerId(UUID answerId) {
        this.answerId = answerId;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
