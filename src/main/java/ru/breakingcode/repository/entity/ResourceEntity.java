package ru.breakingcode.repository.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

@Data
@Entity
@Table(name = "resources")
public class ResourceEntity implements Serializable {
    @EmbeddedId
    protected ResourceEntityPK key;

    public ResourceEntity(ResourceEntityPK key) {
        this.key = key;
    }

    public ResourceEntity() {
    }

    public ResourceEntityPK getKey() {
        return key;
    }

    @Embeddable
    public static class ResourceEntityPK implements Serializable {

        @Column(name = "task_id")
        protected UUID taskId;
        protected String text;

        public ResourceEntityPK() {
        }

        public ResourceEntityPK(UUID taskId,
                                String text) {
            this.taskId = taskId;
            this.text = text;
        }

        public UUID getTaskId() {
            return taskId;
        }

        public String getText() {
            return text;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            ResourceEntityPK that = (ResourceEntityPK) o;
            return taskId.equals(that.taskId) &&
                    text.equals(that.text);
        }

        @Override
        public int hashCode() {
            return Objects.hash(taskId, text);
        }
    }
}
