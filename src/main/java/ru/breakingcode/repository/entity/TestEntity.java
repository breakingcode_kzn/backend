package ru.breakingcode.repository.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Data
@Entity
@Table(name = "tests")
@EqualsAndHashCode(of = "id")
@AllArgsConstructor
@NoArgsConstructor
public class TestEntity {
    @Id
    private UUID id;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "tests_tasks",
            joinColumns = {@JoinColumn(name = "test_id")},
            inverseJoinColumns = {@JoinColumn(name = "task_id")}
    )
    private List<TaskEntity> tasks;
}
