package ru.breakingcode.repository.entity;

import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "answers")
@EqualsAndHashCode(of = "id")
public class AnswerEntity {
    @Id
    private UUID id;

    @JoinColumn(name = "answer_id")
    @OneToMany(fetch = FetchType.EAGER,
            cascade = CascadeType.ALL,
            orphanRemoval = true)
    private List<AnswersValue> values;
    private String details;

    public AnswerEntity() {
    }

    public AnswerEntity(UUID id, String details) {
        this.id = id;
        this.details = details;
    }


    public AnswerEntity(UUID id, List<AnswersValue> values, String details) {
        this.id = id;
        this.values = values;
        this.details = details;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }


    public List<AnswersValue> getValues() {
        return values;
    }

    public void setValues(List<AnswersValue> values) {
        this.values = values;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }
}
