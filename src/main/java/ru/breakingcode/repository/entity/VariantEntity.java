package ru.breakingcode.repository.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "tasks_variants")
@EqualsAndHashCode(of = "id")
@AllArgsConstructor
@NoArgsConstructor
public class VariantEntity {
    @Id
    private UUID id;
    private String description;

    @JoinColumn(name = "answer_id")
    @OneToOne(cascade = CascadeType.ALL,
            fetch = FetchType.EAGER)
    private AnswerEntity answer;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public AnswerEntity getAnswer() {
        return answer;
    }

    public void setAnswer(AnswerEntity answer) {
        this.answer = answer;
    }
}
