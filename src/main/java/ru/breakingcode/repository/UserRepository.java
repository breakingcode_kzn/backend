package ru.breakingcode.repository;

import org.springframework.data.repository.CrudRepository;
import ru.breakingcode.repository.entity.UserEntity;

import java.util.UUID;

public interface UserRepository extends CrudRepository<UserEntity, UUID> {

    UserEntity findByLogin(String login);
}
