package ru.breakingcode.repository;

import org.springframework.data.repository.CrudRepository;
import ru.breakingcode.repository.entity.TaskEntity;

import java.util.UUID;

public interface TaskRepository extends CrudRepository<TaskEntity, UUID> {

}
