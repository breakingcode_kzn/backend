package ru.breakingcode.repository;

import org.springframework.data.repository.CrudRepository;
import ru.breakingcode.repository.entity.TestEntity;

import java.util.UUID;

public interface TestRepository extends CrudRepository<TestEntity, UUID> {

}
