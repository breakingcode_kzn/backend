package ru.breakingcode.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.Resource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.boot.SpringApplication;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.util.FileCopyUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.resource.TransformedResource;
import ru.breakingcode.EntityMapper;
import ru.breakingcode.EntityMapperImpl;
import ru.breakingcode.controller.TestController;
import ru.breakingcode.controller.UserController;
import ru.breakingcode.repository.TaskRepository;
import ru.breakingcode.repository.TestRepository;
import ru.breakingcode.repository.UserRepository;
import ru.breakingcode.service.BreakingcodeUserDetailsService;
import ru.breakingcode.service.TestService;
import ru.breakingcode.service.model.test.Test;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Objects;


@EnableWebMvc
@EntityScan("ru.breakingcode.repository.entity")
@EnableJpaRepositories(
        basePackages = "ru.breakingcode.repository"
)
@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(
                Application.class,
                args
        );
    }

    @Bean
    public EntityMapper entityMapper() {
        return new EntityMapperImpl();
    }

    @Bean
    public BreakingcodeUserDetailsService userService(UserRepository userRepository,
                                                      EntityMapper entityMapper) {
        return new BreakingcodeUserDetailsService(userRepository, entityMapper);
    }

    @Bean
    public TestService testService(TestRepository testRepository,
                                   TaskRepository taskRepository,
                                   EntityMapper entityMapper) {
        return new TestService(testRepository, entityMapper, taskRepository);
    }

    @Bean
    public UserController userController(BreakingcodeUserDetailsService userService) {
        return new UserController(userService);
    }

    @Bean
    public TestController testController(BreakingcodeUserDetailsService userService,
                                         TestService testService) {
        return new TestController(userService, testService);
    }


    @Value("#{environment.HOST_IP}")
    private String hostIp;

    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurer() {
            private final Charset DEFAULT_CHARSET = StandardCharsets.UTF_8;
            private final String BACKEND_SERVER_ADDRESS_PATTERN = "\\%BC_SERVER_ADDRESS_PLACEHOLDER\\%";

            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/api/**").allowedOrigins("*");
                registry.addMapping("/api/**").allowedMethods("GET", "POST", "DELETE", "PUT");
            }

            @Override
            public void addResourceHandlers(ResourceHandlerRegistry registry) {
                registry
                        .addResourceHandler("/**")
                        .addResourceLocations("classpath:/static/")
                        .resourceChain(true)
                        .addTransformer((request, resource, transformerChain) -> {
                            resource = transformerChain.transform(request, resource);

                            String filename = resource.getFilename();

                            class UrlTransformer {
                                TransformedResource transform(Resource res, String pattern, String replaceValue) throws IOException {
                                    byte[] bytes = FileCopyUtils.copyToByteArray(res.getInputStream());
                                    String content = new String(bytes, DEFAULT_CHARSET);
                                    content = content.replaceAll(pattern, replaceValue);
                                    return new TransformedResource(res, content.getBytes(DEFAULT_CHARSET));
                                }
                            }
                            UrlTransformer transformer = new UrlTransformer();
                            if ( "js".equals(StringUtils.getFilenameExtension(filename)) ) {
                                return transformer.transform(
                                        resource,
                                        BACKEND_SERVER_ADDRESS_PATTERN,
                                        (hostIp != null && !hostIp.isEmpty() ? hostIp : "http://localhost:8080")
                                );
                            }
                            return resource;
                        });
            }
        };
    }
}