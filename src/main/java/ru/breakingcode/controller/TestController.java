package ru.breakingcode.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.breakingcode.service.BreakingcodeUserDetailsService;
import ru.breakingcode.service.TestService;
import ru.breakingcode.service.model.test.Test;
import ru.breakingcode.service.model.test.User;

import java.io.IOException;

@RestController
@RequestMapping("api/v1/test")
@Validated
public class TestController {

    private final BreakingcodeUserDetailsService userService;
    private final TestService testService;

    public TestController(BreakingcodeUserDetailsService userService,
                          TestService testService) {
        this.userService = userService;
        this.testService = testService;
    }

    @PostMapping("/start")
    public Test start(@AuthenticationPrincipal User user) throws IOException {
        return testService.createTestByRecomendation(user.getLogin());
    }

    @PostMapping("/result")
    public void results(@AuthenticationPrincipal User user,
                        @RequestBody TestResult testResult) throws IOException {
        testService.results(user.getLogin(), testResult);
    }

    public static class TestResult {

    }
}
