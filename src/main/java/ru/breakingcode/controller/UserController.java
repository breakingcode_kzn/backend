package ru.breakingcode.controller;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ru.breakingcode.service.BreakingcodeUserDetailsService;
import ru.breakingcode.service.model.test.User;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("api/v1")
@Validated
public class UserController {

    private final BreakingcodeUserDetailsService userService;

    public UserController(BreakingcodeUserDetailsService userService) {
        this.userService = userService;
    }

    @PostMapping("/register")
    public void register(@Valid @NotNull(message = "{validation.user.empty}") @RequestBody User user) {
        userService.createNewUser(user);
    }

//    @PostMapping("/login")
//    public void login(@Valid @NotNull(message = "{validation.user.empty}") @RequestBody Credentialas credentials) {
//        userService.login(credentials.getLogin(), credentials.getPassword());
//    }

    public static class Credentialas {
        private final String login;
        private final String password;

        public Credentialas(String login, String password) {
            this.login = login;
            this.password = password;
        }

        public String getLogin() {
            return login;
        }

        public String getPassword() {
            return password;
        }
    }
}
