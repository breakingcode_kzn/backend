package ru.breakingcode;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.breakingcode.repository.entity.*;
import ru.breakingcode.service.model.test.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Mapper
public interface EntityMapper {
    @Mapping(source = "role", target = "role.authority")
    User mapToUser(UserEntity entity);

    List<User> mapToUserEntity(Iterable<UserEntity> entity);

    @Mapping(source = "role.authority", target = "role")
    UserEntity mapToEntity(User entity);

    TestEntity map(Test test);

    Test map(TestEntity test);

    default TaskEntity mapTask(Task task) {
        if (task == null) {
            return null;
        }
        TaskEntity taskEntity = new TaskEntity();
        taskEntity.setId(task.getId());
        taskEntity.setName(task.getName());
        taskEntity.setNumber(task.getNumber());

        List<ResourceEntity> resources = new ArrayList<>();
        if (task.getResources() != null) {
            for (String resource : task.getResources()) {
                resources.add(new ResourceEntity(
                        new ResourceEntity.ResourceEntityPK(
                                task.getId(),
                                resource
                        )
                ));
            }

        }

        taskEntity.setResources(resources);
        taskEntity.setVariants(mapVariant(task.getVariants()));

        return taskEntity;
    }

    default Task mapTaskEntity(TaskEntity taskEntity) {
        if (taskEntity == null) {
            return null;
        }
        Task task = new Task();
        task.setId(taskEntity.getId());
        task.setName(taskEntity.getName());
        task.setNumber(taskEntity.getNumber());

        List<String> resources = new ArrayList<>();
        if (taskEntity.getResources() != null) {
            for (ResourceEntity resource : taskEntity.getResources()) {
                resources.add(
                        resource.getKey().getText()
                );
            }

        }

        task.setResources(resources);
        task.setVariants(mapEntityVariant(taskEntity.getVariants()));

        return task;
    }

    List<TaskEntity> mapTask(Iterable<Task> task);

    List<Task> mapTaskEntity(Iterable<TaskEntity> entity);

    default VariantEntity mapVariant(Variant variant) {
        if (variant == null) {
            return null;
        }
        VariantEntity entity = new VariantEntity();
        entity.setId(variant.getId());
        entity.setDescription(variant.getDescription());

        if (variant.getAnswer() != null) {

            entity.setAnswer(new AnswerEntity(
                    variant.getId(),
                    variant.getAnswer().getValues().stream()
                            .map(value -> new AnswersValue(variant.getId(), value))
                            .collect(Collectors.toList()),
                    variant.getAnswer().getDetails()
            ));
        }

        return entity;
    }

    default Variant mapEntityVariant(VariantEntity entity) {
        if (entity == null) {
            return null;
        }
        Variant variant = new Variant();
        variant.setId(entity.getId());
        variant.setDescription(entity.getDescription());

        if (entity.getAnswer() != null) {

            variant.setAnswer(new Answer(
                    entity.getAnswer().getValues().stream()
                            .map(AnswersValue::getValue)
                            .collect(Collectors.toList()),
                    entity.getAnswer().getDetails()
            ));
        }

        return variant;
    }

    List<VariantEntity> mapVariant(List<Variant> variant);

    List<Variant> mapEntityVariant(List<VariantEntity> variant);
}
