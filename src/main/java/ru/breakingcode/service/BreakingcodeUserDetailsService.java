package ru.breakingcode.service;

import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import ru.breakingcode.EntityMapper;
import ru.breakingcode.repository.UserRepository;
import ru.breakingcode.service.model.test.User;

public class BreakingcodeUserDetailsService implements UserDetailsService {
    private final UserRepository userRepository;
    private final EntityMapper mapper;

    public BreakingcodeUserDetailsService(UserRepository userRepository,
                              EntityMapper mapper) {
        this.userRepository = userRepository;
        this.mapper = mapper;
    }

    @Override
    public User loadUserByUsername(String username) throws UsernameNotFoundException {
        return mapper.mapToUser(userRepository.findByLogin(username));
    }

    public void createNewUser(User user) {
        userRepository.save(mapper.mapToEntity(user));
    }
}
