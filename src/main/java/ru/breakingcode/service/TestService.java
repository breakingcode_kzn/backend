package ru.breakingcode.service;

import ru.breakingcode.EntityMapper;
import ru.breakingcode.controller.TestController;
import ru.breakingcode.repository.TaskRepository;
import ru.breakingcode.repository.TestRepository;
import ru.breakingcode.repository.entity.TestEntity;
import ru.breakingcode.service.model.test.Task;
import ru.breakingcode.service.model.test.Test;

import javax.transaction.Transactional;
import java.util.List;

public class TestService {
    private final TestRepository repository;
    private final EntityMapper mapper;
    private final TaskRepository taskRepository;

    public TestService(TestRepository repository,
                       EntityMapper mapper,
                       TaskRepository taskRepository) {
        this.repository = repository;
        this.mapper = mapper;
        this.taskRepository = taskRepository;
    }

    @Transactional
    public void save(Test test) {
        List<Task> tasks = test.getTasks();
        taskRepository.saveAll(mapper.mapTask(tasks));

        repository.save(
                mapper.map(test)
        );
    }

    public boolean isEmpty() {
        return repository.count() < 1;
    }

    @Transactional
    public Test createTestByRecomendation(String userLogin) {
        // TODO: реализовать генерацию теста по рекомендации
        Iterable<TestEntity> all = repository.findAll();
        return mapper.map(all.iterator().next());
    }

    public void results(String userLogin, TestController.TestResult testResult) {

    }
}
