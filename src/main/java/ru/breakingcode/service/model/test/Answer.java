package ru.breakingcode.service.model.test;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

public class Answer {
    private List<String> values;
    private String details;

    public Answer() {
    }

    public Answer(List<String> values, String details) {
        this.values = values;
        this.details = details;
    }

    public List<String> getValues() {
        return values;
    }

    public void setValues(List<String> values) {
        this.values = values;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }
}
