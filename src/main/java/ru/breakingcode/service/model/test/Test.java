package ru.breakingcode.service.model.test;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.UUID;


@Data
@NoArgsConstructor
public class Test {
    private UUID id;
    private List<Task> tasks;

    public Test(UUID id, List<Task> tasks) {
        this.id = id;
        this.tasks = tasks;
    }
}
