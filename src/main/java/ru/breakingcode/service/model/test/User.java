package ru.breakingcode.service.model.test;

import lombok.*;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import ru.breakingcode.service.model.BreakingcodeAuthority;

import java.util.Arrays;
import java.util.Collection;
import java.util.UUID;

@Setter
@EqualsAndHashCode(of = "uuid")
@AllArgsConstructor
@NoArgsConstructor
public class User implements UserDetails {
    private UUID uuid;
    private String login;
    private String name;
    private String surname;
    private String password;
    private BreakingcodeAuthority role;

    public BreakingcodeAuthority getRole() {
        return role;
    }

    public UUID getUuid() {
        return uuid;
    }

    public String getLogin() {
        return login;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Arrays.asList(role);
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return login;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}