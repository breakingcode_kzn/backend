package ru.breakingcode.service.model;

import org.springframework.security.core.GrantedAuthority;


public class BreakingcodeAuthority implements GrantedAuthority {
    private String authority;

    public BreakingcodeAuthority(String authority) {
        this.authority = authority;
    }

    public BreakingcodeAuthority() {
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }

    @Override
    public String getAuthority() {
        return authority;
    }
}
