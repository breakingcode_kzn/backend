package ru.breakingcode.service;

import ru.breakingcode.EntityMapper;
import ru.breakingcode.repository.TaskRepository;
import ru.breakingcode.service.model.test.Task;

import java.util.List;

public class TaskService {
    private final TaskRepository taskRepository;
    private final EntityMapper mapper;

    public TaskService(TaskRepository taskRepository,
                       EntityMapper mapper) {
        this.taskRepository = taskRepository;
        this.mapper = mapper;
    }

    public List<Task> getAllTasks() {
        return mapper.mapTaskEntity(
                taskRepository.findAll()
        );
    }
}
