FROM maven:3.6.1-jdk-8-slim as builder
COPY --from=registry.gitlab.com/breakingcode_kzn/frontend/frontend /build-dir/build /build-dir/target/classes/static/


USER root
WORKDIR /build-dir
ADD / ./
RUN mvn install

# ----

FROM java:8-jre

COPY --from=builder /build-dir/target/*exec.jar /breakingcode/

EXPOSE 8080
WORKDIR /breakingcode

CMD java -Djava.security.egd=file:/dev/./urandom -jar *.jar