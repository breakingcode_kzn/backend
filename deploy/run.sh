#!/bin/bash

docker run -it -v $(pwd)/breakingcode.yml:/ansible/breakingcode.yml -v $(pwd)/:/etc/ansible/ --rm=true registry.gitlab.com/breakingcode_kzn/backend/ansible ansible-playbook breakingcode.yml --ssh-common-args="-o StrictHostKeyChecking=no" --key-file=/root/.ssh/bc_docker_id_rsa