#!/usr/bin/env bash

docker run --rm \
--name postgres \
-p 5432:5432 \
-e POSTGRES_PASSWORD=postgres \
-e POSTGRES_DB=breakingcode \
-d \
postgres:9.6.10
